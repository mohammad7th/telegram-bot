// Dependencies
const express = require('express');
const app = express();
const axios = require('axios');
const bodyParser = require('body-parser');
const port = 80;
const url = 'https://api.telegram.org/bot';
const apiToken = '5828103474:AAFuJpTYcypEeqigevSOQhGSj8qjO8go4dU';
// Configurations
app.use(bodyParser.json());
// Endpoints
app.post('/', (req: any, res: any) => {
    // console.log(req.body);
    const chatId = req.body.message.chat.id;
    console.log('chatId', chatId);

    const sentMessage = req.body.message.text;
    console.log('sentMessage', sentMessage);

    // Regex for hello
    if (sentMessage.match(/hello/gi)) {
        axios.post(`${url}${apiToken}/sendMessage`,
            {
                chat_id: chatId,
                text: 'hello back 👋'
            })
            .then((response: any) => {
                console.log('then', response);

                res.status(200).send(response);
            }).catch((error: any) => {
                console.log('catch', error);

                res.send(error);
            });
    } else {
        // if no hello present, just respond with 200 
        res.status(200).send({});
    }
});
// Listening
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});