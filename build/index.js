"use strict";
// Dependencies
var express = require('express');
var app = express();
var axios = require('axios');
var bodyParser = require('body-parser');
var port = 80;
var url = 'https://api.telegram.org/bot';
var apiToken = '5828103474:AAFuJpTYcypEeqigevSOQhGSj8qjO8go4dU';
// Configurations
app.use(bodyParser.json());
// Endpoints
app.post('/', function (req, res) {
    // console.log(req.body);
    var chatId = req.body.message.chat.id;
    console.log('chatId', chatId);
    var sentMessage = req.body.message.text;
    console.log('sentMessage', sentMessage);
    // Regex for hello
    if (sentMessage.match(/hello/gi)) {
        axios.post("".concat(url).concat(apiToken, "/sendMessage"), {
            chat_id: chatId,
            text: 'hello back 👋'
        })
            .then(function (response) {
            console.log('then', response);
            res.status(200).send(response);
        }).catch(function (error) {
            console.log('catch', error);
            res.send(error);
        });
    }
    else {
        // if no hello present, just respond with 200 
        res.status(200).send({});
    }
});
// Listening
app.listen(port, function () {
    console.log("Listening on port ".concat(port));
});
